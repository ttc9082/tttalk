# TT.Talk #

This is a website I've been making for noting coding technics and displaying some previous works. Most importantly, I can practice coding skills before I find a job. Compared with my old works, this project is more focusing on front end.
I also added a section of showing some songs I covered. with a simple and beautiful js music player I designed.

### Features ###

* Markdown syntax and Code highlighting
* Categories and types
* Tag searching, pagination, view counter
* Insert music for articles, music player
* Comments(not yet)
* Portfolio display(not yet)

### Screenshots ###

![1.png](https://bitbucket.org/repo/rA6xao/images/2283297991-1.png)

* **Main layout**

![2.png](https://bitbucket.org/repo/rA6xao/images/1534540859-2.png)

* JS control Music player

![3.png](https://bitbucket.org/repo/rA6xao/images/3328922272-3.png)

* Markdown style and code highlighter.

![4.png](https://bitbucket.org/repo/rA6xao/images/1122734144-4.png)

* Admin powered by django-admin and Grappelli and Epic-editor