from django.contrib import admin
from .models import *
from epiceditor.widgets import AdminEpicEditorWidget
# Register your models here.


class TagAdminInline(admin.TabularInline):
    model = Tag


class ArticleEd(admin.ModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': AdminEpicEditorWidget(attrs={'rows': 40, 'cols': 40})},
    }
    inlines = (TagAdminInline, )
    list_display = ['title', 'datetime', 'category', 'picture', 'views', 'project']
    ordering = ['-datetime']


class TitleEd(admin.ModelAdmin):
    list_display = ['title']

admin.site.register(Article, ArticleEd)
admin.site.register(Category, TitleEd)
admin.site.register(Type, TitleEd)
admin.site.register(Tag, TitleEd)
admin.site.register(Music, TitleEd)
admin.site.register(Project, TitleEd)