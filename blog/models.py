from django.db import models


# Create your models here.
class Music(models.Model):
    title = models.CharField(max_length=50)
    author = models.CharField(max_length=50)
    url = models.TextField()

    def __unicode__(self):
        return self.title


class Category(models.Model):
    title = models.CharField(max_length=50)

    def __unicode__(self):
        return self.title


class Type(models.Model):
    title = models.CharField(max_length=50)
    icon = models.CharField(max_length=20)

    def __unicode__(self):
        return self.title


class Project(models.Model):
    title = models.CharField(max_length=200)
    description = models.CharField(max_length=5000)
    picture = models.TextField(blank=True)
    demo_url = models.TextField(blank=True)
    code_url = models.TextField(blank=True)
    git = models.CharField(max_length=20, choices=(('bitbucket', 'bitbucket'),('github', 'github')))

    def __unicode__(self):
        return self.title


class Article(models.Model):
    title = models.CharField(max_length=200)
    datetime = models.DateField()
    content = models.TextField()
    summary = models.TextField(max_length=200)
    picture = models.TextField(blank=True)
    views = models.IntegerField(default=0)
    category = models.ForeignKey(Category)
    type = models.ForeignKey(Type)
    music = models.ForeignKey(Music, null=True, blank=True)
    project = models.ForeignKey(Project, null=True, blank=True)

    def __unicode__(self):
        return self.title


class Tag(models.Model):
    title = models.CharField(max_length=50)
    Article = models.ForeignKey(Article)

    def __unicode__(self):
        return self.title
