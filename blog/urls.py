from django.conf.urls import patterns, include, url


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'TTTalk.views.home', name='home'),
    url(r'^$','blog.views.show_entries', name='home'),
    url(r'^(?P<page>[0-9]+)/$', 'blog.views.show_entries', name='show'),
    url(r'^article/(?P<entry_id>[0-9]+)$', 'blog.views.entry_detail', name='article'),
)