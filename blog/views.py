from django.shortcuts import render
from models import *
import operator


def show_entries(request, page=1, keywords=None):
    context = {}
    page = int(page)
    if request.GET:
        keywords = request.GET['search']
    if not keywords:
        articles = Article.objects.all()
    else:
        articles = []
        art_dict = {}
        for key in keywords.split(' '):
            tmp = Article.objects.filter(tag__title__iexact=key)
            print 'tmp=', tmp
            for art in tmp:
                if art in art_dict:
                    art_dict[art] += 1
                else:
                    art_dict[art] = 1
                    print 'added'
        sorted_art = sorted(art_dict.iteritems(), operator.itemgetter(1))  # Sorted the articles by relevance
        sorted_art.reverse()  # Sort in descending order
        print sorted_art
        if sorted_art:
            for art_tuple in sorted_art:
                articles.append(art_tuple[0])
        print articles
    art_len = len(articles)
    pages = art_len / 8 + 1  # Get page number of all articles.
    cur_arts = articles[(page-1)*8:page*8-1]  # Get 8 articles from the current page
    context['pages'] = range(pages)  # Total number of pages
    context['page'] = page  # Current page number
    context['articles'] = cur_arts
    context['keywords'] = keywords
    return render(request, 'blog.html', context)


def entry_detail(request, entry_id=0):
    context = {}
    entry = Article.objects.get(pk=entry_id)
    if entry_id not in request.session:
        entry.views += 1
        entry.save()
        request.session[entry_id] = True
    context['article'] = entry
    return render(request, 'entry.html', context)