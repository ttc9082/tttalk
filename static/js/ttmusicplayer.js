$('document').ready(function(){
    $controlBtn = $('#controlBtn');
    $musicplayer = $('#musicplayer')[0];
    $controlIcon = $('#controlIcon');
    $progressBar = $('#progressBar');
    $defaultBar = $('#defaultBar');
    $captain = $('#captain');

$controlBtn.on('click', function(){
//   alert('Clicked');
   if($musicplayer.paused || $musicplayer.ended){
       $musicplayer.play();
       $controlIcon.removeClass('play').addClass('pause');
       $(this).addClass('clicked');
       updatetime = setInterval(update, 500);
   }
   else{
       $musicplayer.pause();
       $controlIcon.removeClass('pause').addClass('play');
       $(this).removeClass('clicked');
       window.clearInterval(updatetime);
   }
});

function update(){
    if(!$musicplayer.ended){
        var currentMin = parseInt($musicplayer.currentTime/60),
            currentSec = parseInt($musicplayer.currentTime%60);
        percentage = $musicplayer.currentTime/$musicplayer.duration*100;
        $progressBar.css('width', percentage+'%');
    }
    else{
        $controlIcon.removeClass('pause').addClass('play');
        $controlBtn.removeClass('clicked');
        $progressBar.css('width', '0');
        window.clearInterval(updatetime);
    }
}

$defaultBar.on('mouseover', function(e){
//    alert(e.pageX - defaultBar.offsetLeft);
});

$defaultBar.on('click', function(e){
    var time = (e.pageX - defaultBar.offsetLeft)/$defaultBar.width()*$musicplayer.duration;
    $musicplayer.currentTime = time;
    update();
});

$captain.on('click', function(e){
    var time = (e.pageX - defaultBar.offsetLeft)/$defaultBar.width()*$musicplayer.duration;
    $musicplayer.currentTime = time;
    update();
});



});